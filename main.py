#!/usr/bin/env python3
"""A scrapping script that fetches webradios references."""


import asyncio
import base64
from datetime import datetime
from io import BytesIO
import logging
from collections import namedtuple
import os
import re
import shutil

import aiohttp
from lxml import etree


logging.basicConfig(
    format='%(levelname)s - %(asctime)s - %(message)s',
    datefmt='%I:%M:%S',
    level=logging.INFO)

B64URL = "aHR0cDovL3Z0dW5lci5jb20vc2V0dXBhcHAvZ3VpZGUvYXNw"
BASE_URL = base64.b64decode(B64URL).decode('utf-8')

WEBRADIO_SEMAPHORE = asyncio.Semaphore(10)
STYLE_SEMAPHORE = asyncio.Semaphore(10)
SLEEP_TIME = 0


NumberedPage = namedtuple("NumberedPage", ["number", "url"])

def format_url(url):
    """Returns an absolute url given a relative url."""
    return BASE_URL + url[2:]


def format_text(text):
    """Returns a correctly formatted string."""
    return text.replace(" ", "_")


class WebPage:
    """Mother-class for page representation."""

    # As you will be blacklisted after around 15k parses,
    # we need to add a few http proxies here (I think 5 is nice)
    # aiohttp only allows http proxies.
    proxies = [
        "http://85.43.127.116:80",
        "http://194.124.35.57:80",
        "http://194.167.44.91:80",
        "http://62.159.156.142:80",
        "http://91.205.174.26:80",
        "http://146.255.109.129:80",
    ]

    headers = {
        "User-Agent": (
            "Mozilla/5.0 (X11; Debian; Linux x86_64; rv:52.0) "
            "Gecko/20100101 Firefox/58.0"
        )
    }

    def __init__(self, url):
        self.url = url
        self.tree = None

    async def get_tree(self):
        """Returns the xpath tree corresponding to the MusicStylePage url."""
        kwargs = {
            "headers": self.headers,
            "proxy": self.proxies[0]
        }
        async with aiohttp.ClientSession() as session:
            async with session.get(self.url, **kwargs) as response:
                if response.status >= 500:
                    self.proxies.pop(0)
                    logging.warning(
                        "%s proxies remaining in PROXIES", len(self.proxies))
                    return await self.get_tree()
                memfile = BytesIO(await response.read())
                await asyncio.sleep(SLEEP_TIME)
                parser = etree.HTMLParser()
                return etree.parse(memfile, parser)

    async def parse(self):
        """Parse the page to instanciate the representation."""
        if not self.proxies:
            logging.error("No valid proxy in PROXIES.")
            self.tree = None
            return
        self.tree = await self.get_tree()


class PaginatedPage(WebPage):
    """Mother-class for paginated page representation."""

    xpath_page_number = """//font[@color="red"]/b"""
    xpath_pages = """//a[@class="paging"]"""

    def __init__(self, url):
        super(PaginatedPage, self).__init__(url)
        self.current_page = None
        self.next_page = None

    async def parse(self):
        await super(PaginatedPage, self).parse()
        self.current_page = self.get_current_page()
        self.next_page = self.get_next_page()

    def get_current_page(self):
        """Returns the current page number if a pager exists"""
        string_page = self.tree.xpath(self.xpath_page_number)
        if string_page:
            return int(string_page[0].text)
        return None

    def get_pages(self):
        """returns a list of tuples [(page_num, page_url)]"""
        res = []
        pages = self.tree.xpath(self.xpath_pages)
        for page in pages:
            url = format_url(page.attrib["href"])
            num = int(page.getchildren()[0].text)
            res.append(NumberedPage(num, url))
        return res

    def get_next_page(self):
        """Returns the next page if a pager exists"""
        if self.current_page:
            pages = self.get_pages()
            next_pages = [
                page for page in pages if page.number > self.current_page
            ]
            if next_pages:
                sorted_pages = sorted(next_pages, key=lambda p: p[0])
                return sorted_pages[0]
        return None


class Category:
    """Mother-class for category classes."""

    def __init__(self, name):
        self.name = name
        self.webradios = []
        self.filename = self.name + ".m3u" # default filename

    def write(self):
        """Creates a file containing webradios informations."""
        with open(self.filename, "w") as f_obj:
            f_obj.write("#EXTM3U\n")
            for webradio in self.webradios:
                f_obj.write("#EXTINF:-1,%s\n" % webradio.name)
                f_obj.write("%s\n" % webradio.stream_url)

    def add(self, webradio):
        """Add a webradio"""
        self.webradios.append(webradio)


class Country(Category):
    """Container class that contains each webradio for a given country."""
    folder_name = "Countries"

    def __init__(self, name):
        super(Country, self).__init__(name)
        self.filename = os.path.join(self.folder_name, name + ".m3u")


class Genre(Category):
    """Container class that contains each webradio for a given genre."""
    folder_name = "Genres"

    def __init__(self, name):
        super(Genre, self).__init__(name)
        self.filename = os.path.join(self.folder_name, name + ".m3u")


class Language(Category):
    """Container class that contains each webradio for a given language."""
    folder_name = "Languages"

    def __init__(self, name):
        super(Language, self).__init__(name)
        self.filename = os.path.join(self.folder_name, name + ".m3u")


class WebRadio(WebPage):
    """web radio representation."""

    mp3_regex = re.compile("mp3.*")
    geoblocked_regex = re.compile(".*geoblocked.*")
    xpath_country = """//span[@id="StatCountry"]"""
    xpath_genre = """//span[@id="StatGenre"]"""
    xpath_name = """//span[@id="StatName"]"""
    xpath_script = """//script[preceding-sibling::*[1][self::img]]"""

    def __init__(self, url, language):
        super(WebRadio, self).__init__(url)
        self.name = None
        self.language = language
        self.country = None
        self.genres = None
        self.stream_url = None
        self.geoblocked = False

    def get_name(self):
        """Returns the webradio name from the html tree."""
        name = self.tree.xpath(self.xpath_name)[0].text
        return format_text(name)

    def get_country(self):
        """Returns the coutry name from the html tree."""
        country = self.tree.xpath(self.xpath_country)[0].text
        return format_text(country)

    def get_genres(self):
        """Returns the actual genre of the webradio given a page."""
        genres = self.tree.xpath(self.xpath_genre)[0].text
        return [format_text(g) for g in genres.split("/")]

    def get_url_stream(self):
        """Returns the actual webradio url from a given a page."""
        script = self.tree.xpath(self.xpath_script)[0]
        string_tree = etree.tostring(script).decode().replace("&#13;", "")
        tree_lines = string_tree.split("\n")
        mp3_lines = [
            line.strip() for
            line in tree_lines if
            self.mp3_regex.match(line.strip())
        ]
        mp3_line = mp3_lines[0]
        return mp3_line.split()[1].replace('"', "")

    async def parse(self):
        await super(WebRadio, self).parse()
        self.stream_url = self.get_url_stream()
        geoblocked_regex = re.compile(self.geoblocked_regex)
        if geoblocked_regex.match(self.stream_url):
            self.geoblocked = True
        self.country = self.get_country()
        self.genres = self.get_genres()
        self.name = self.get_name()


class LanguagePage(PaginatedPage):
    """Music style representation."""

    xpath_webradios = """//td[@class="links"][@width="290"]/a"""

    def __init__(self, url, name=None):
        super(LanguagePage, self).__init__(url)
        self.name = name
        self.webradios = {}

    async def get_webradio(self, elem):
        """Returns WebRadio instance given a corresponding page element."""
        async with WEBRADIO_SEMAPHORE:
            formatted_url = format_url(elem.attrib["href"])
            webradio = WebRadio(formatted_url, self.name)
            await webradio.parse()
            return webradio

    async def get_webradios(self):
        """Returns a list or WebRadio instances"""
        elems = self.tree.xpath(self.xpath_webradios)
        tasks = [self.get_webradio(elem) for elem in elems]
        return await asyncio.gather(*tasks)

    async def parse(self):
        """returns a dict {"name": "url"} or webradios given an html doc"""
        await super(LanguagePage, self).parse()
        if self.current_page:
            logging.info("%s - parsing page %s", self.name, self.current_page)
        self.webradios = await self.get_webradios()
        if self.next_page:
            next_category = LanguagePage(self.next_page.url, self.name)
            await next_category.parse()
            self.webradios.extend(next_category.webradios)
        if self.current_page == 1 or not self.current_page:
            logging.info(
                "%s webradios parsed in category %s",
                len(self.webradios), self.name
            )

class LanguageWrapperPage(WebPage):
    """As WebRadios aren't directly accessible from language pages,
    we need an intermediate layer."""

    xpath_language = (
        """//td[@class="links"]/a[contains(text(), "All Stations")]""")

    def __init__(self, url, name):
        super(LanguageWrapperPage, self).__init__(url)
        self.name = name
        self.language = None
        self.webradios = None
        self.tries = 0

    def make_language(self):
        """Returns a LanguagePage given a LanguageWrapperPage html tree."""
        elem = self.tree.xpath(self.xpath_language)[0]
        language_url = elem.attrib["href"]
        return LanguagePage(format_url(language_url), self.name)

    async def parse(self):
        await super(LanguageWrapperPage, self).parse()
        self.tries += 1
        self.language = self.make_language()
        await self.language.parse()
        self.webradios = self.language.webradios

class LanguagesPage(WebPage):
    """ "browse by" representation."""

    suffix_url = "/BrowseStations/StartPage.asp?sBrowseType=Language"
    xpath_wrapper = """//td[@class="links"]/a"""

    def __init__(self):
        self.url = BASE_URL + self.suffix_url
        super(LanguagesPage, self).__init__(self.url)
        self.webradios = []
        self.genres = {}
        self.countries = {}
        self.languages = {}

    def group_webradios(self):
        """Sorts webradios by category (genre, country, language)."""
        for webradio in self.webradios:
            if webradio.geoblocked:
                continue
            for genre in webradio.genres:
                self.genres.setdefault(genre, Genre(genre)).add(webradio)
            self.countries.setdefault(
                webradio.country, Country(webradio.country)).add(webradio)
            self.languages.setdefault(
                webradio.language, Language(webradio.language)).add(webradio)

    def make_directories(self):
        """Creates all required directories."""
        self.create_or_replace_dir(Country.folder_name)
        self.create_or_replace_dir(Language.folder_name)
        self.create_or_replace_dir(Genre.folder_name)

    def create_or_replace_dir(self, name):
        """Creates a dir given a name"""
        try:
            os.mkdir(name)
        except OSError:
            shutil.rmtree(name)
            os.mkdir(name)

    async def parse(self):
        """Entry point."""
        await super(LanguagesPage, self).parse()
        self.make_directories()
        await self.populate()
        self.group_webradios()
        self.write()

    async def make_wrapper(self, elem):
        """Returns a LanguageWrapperPage instance given its
        corresponding page element."""
        async with STYLE_SEMAPHORE:
            url = format_url(elem.attrib["href"])
            name = "_".join(elem.text.split()[:-1])
            wrapper = LanguageWrapperPage(url, name)
            await wrapper.parse()
            return wrapper

    def write(self):
        """Write all webradios in group folders"""
        for genre in self.genres.values():
            genre.write()
        for language in self.languages.values():
            language.write()
        for country in self.countries.values():
            country.write()

    async def populate(self):
        """Gets a list of all webradios."""
        elems = self.tree.xpath(self.xpath_wrapper)
        tasks = [self.make_wrapper(elem) for elem in elems]
        wrappers = await asyncio.gather(*tasks)
        for wrapper in wrappers:
            self.webradios.extend(wrapper.webradios)

if __name__ == "__main__":
    start = datetime.now()
    root_page = LanguagesPage()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(root_page.parse())
    end = datetime.now()
    logging.info(end - start)
