with import <nixpkgs> {};
with python3Packages;

buildPythonPackage rec {
  name = "webradio-parser";
  src = ./.;
  propagatedBuildInputs = [
    aiohttp
    lxml
  ];
}
